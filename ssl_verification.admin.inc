<?php


/**
 * Form builder function for the generap SSL Verification configuration
 */
function ssl_verification_admin_settings() {

  $form['ssl_verification_count_type'] = array(
    '#type' => 'radios',
    '#title' => t('Challenge rule'),
    '#description' => t("Define the type of count down a user has to pass to be asked for verifying the SSL certificate's fingerprint"),
    '#default_value' => variable_get('ssl_verification_count_type', 'logins'),
    '#options' => array(
      'logins' => t('Amount of logins'),
      'time' => t('Certain time'),
    ),
  );

  $form['ssl_verification_login_count'] = array(
    '#type' => 'select',
    '#title' => t('Login count'),
    '#description' => t("Define the amount of logins a user has to pass to be asked for verifying the SSL certificate's fingerprint"),
    '#default_value' => variable_get('ssl_verification_login_count', 10),
    '#states' => array(
      'visible' => array(
        'input[name=ssl_verification_count_type]' => array('value' => 'logins'),
      ),
    ),
    '#options' => array(
      1 => 1,
      2 => 2,
      3 => 3,
      5 => 5,
      10 => 10,
      20 => 20,
      50 => 50,
      100 => 100,
      200 => 200,
      500 => 500
    ),
  );

  $form['ssl_verification_date_count'] = array(
    '#type' => 'select',
    '#title' => t('Amount of time'),
    '#description' => t("Define the time that has to past in order to ask the use to verify the SSL certifacte's fingerprint"),
    '#default_value' => variable_get('ssl_verification_date_count', 7),
    '#states' => array(
      'visible' => array(
        'input[name=ssl_verification_count_type]' => array('value' => 'time'),
      ),
    ),
    '#options' => array(
      1 => t('1 day'),
      2 => t('2 days'),
      3 => t('3 days'),
      5 => t('5 days'),
      7 => t('1 week'),
      14 => t('2 weeks'),
      21 => t('3 weeks'),
      28 => t('4 weeks'),
      60 => t('2 months'),
      90 => t('3 months'),
    ),
  );

  return system_settings_form($form);
}

