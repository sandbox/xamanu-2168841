<?php


/**
 * Page callback to SSL Verification
 */
function ssl_verification_form() {

  global $user;

  $form = array();

  $fingerprint = ssl_verification_get_fingerprint();

  if ($fingerprint['result']) {

    $form['instructions'] = array(
      '#markup' => t('Plase verify the fingerprint of the SSL certificate'),
    );

    $form['fingerprint'] = array(
      '#markup' => theme('ssl_verification_fingerprint', $fingerprint),
    );

    $form['actions'] = array(
      '#type' => 'actions',
      '#weight' => 20,

    );
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Correct'),
    );
   $form['actions']['fail'] = array(
      '#type' => 'submit',
      '#value' => t('Not correct'),
    );

    $form['browsers'] = array(
      '#prefix' => '<h2>' . t('Instructions') . '</h2>',
      '#markup' => theme('ssl_verification_help'),
      '#weight' => 99,
    );
  }
  else {
    $form['fingerprint'] = array(
      '#markup' => $fingerprint['message'],
    );
  }
  return $form;
}

/**
 * Sumit function for ssl verification form.
 */
function ssl_verification_form_submit($form, $form_state) {

  if ($form_state['values']['op'] == $form_state['values']['submit']) {
    ssl_verification_save_response('positive');
    drupal_set_message(t("Thank you for confirming the ssl key's fingerprint. This helps us to increase the security of your connection."), 'status');
    drupal_goto();
  }
  else {
    ssl_verification_save_response('negative');
    drupal_set_message(t('The fingerprint of the ssl key was not confirmed. You are probably victim of an attack. For your own security your account has been blocked. Please do not use the website anymore and get in contact with the administrators.'), 'error');
  }
}

  //}

/**
 * Function to obtain the fingerprint of the host.
 */
function ssl_verification_get_fingerprint() {

  // Check if SSL is used
  //if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) {
  if(TRUE) {
    // Get connection information together.
    global $base_url;
    $url = preg_replace('#^https?://#', '', $base_url);
    $url = 'activismoenred.net';
    $port = 443;

    // Open socket to read SSL certifate.
    $context = stream_context_create(array("ssl" => array("capture_peer_cert" => true)));
    $socket = @stream_socket_client("ssl://$url:$port", $errno, $errstr, ini_get("default_socket_timeout"), STREAM_CLIENT_CONNECT, $context);

    // No certificate could be obtained.
    if(!$socket) {
      return array(
        'result' => FALSE,
        'message' => t('Certificate could not be obtained'),
      );
    }

    // Obtaining the certificate.
    $contextdata = stream_context_get_params($socket);
    $contextparams = $contextdata['options']['ssl']['peer_certificate'];
    fclose($socket);

    // Prepare certificate.
    openssl_x509_export($contextparams, $cert, true);
    openssl_x509_free($contextparams);
    $repl = array("\r", "\n", "-----BEGIN CERTIFICATE-----", "-----END CERTIFICATE-----");
    $repw = array("", "", "", "");
    $cert = str_replace($repl, $repw, $cert);
    $decoded = base64_decode($cert);

    // Build and return decoded fingerprint information.
    return array(
      'result' => TRUE,
      "md5" => strtoupper(md5($decoded)),
      "sha1" => strtoupper(sha1($decoded)),
    );
  }

  // No SSL here.
  else {
    return array(
      'result' => FALSE,
      'message' => t('Not on a SSL site'),
    );
  }
}


/**
 * Function to save respone data
 */
function ssl_verification_save_response($type = FALSE) {

  global $user;
  if (is_string($type)) {

    if ($type == 'positive') {

      // Get information about the user's ssl verification
      $result = db_query('SELECT * FROM {ssl_verification} sv WHERE sv.uid = :uid', array(':uid' => $user->uid))->fetchAssoc();

      // Obtain inicial login count
      $result['countdown'] = variable_get('ssl_verification_login_count', 10);

      // Set the timestamp to now
      $result['timestamp']  = time();

      // Save data to database
      drupal_write_record('ssl_verification', $result, 'uid');
      return;
    }

    elseif ($type == 'negative') {

      // Block user
      $user->status = 0;
      user_save($user);

      return;
    }
  }
  watchdog('ssl_verification', 'A SSL verification response could not be saved into the database');
}
