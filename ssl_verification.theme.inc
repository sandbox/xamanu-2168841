<?php

/**
 * Provides text for the user how to check fingerprints on different browsers.
 */
function theme_ssl_verification_help($browsers = NULL) {
  drupal_add_js('misc/form.js');
  drupal_add_js('misc/collapse.js');

  global $base_root;
  global $base_path;
  $path =  $base_root . $base_path . drupal_get_path('module', 'ssl_verification');

  // Firefox
  $output = '
    <fieldset id="ssl-verification-firefox" class="collapsible collapsed">
      <legend><span class="fieldset-legend"><img src="' . $path . '/images/logos/firefox.png" title="Firefox" alt="Firefox"/></span></legend>
      <div class="fieldset-wrapper">
        <div class="fieldset-description"></div>
        <div class="step-1">
          <h4>' . t('Step %number', array('%number' => 1)) .'</h4><br />
          <img border="1" src="' . $path . '/images/howto/firefox/dropdown.png"/>
          <p>' . t('Click on the little lock symbol left to the url in the navigation bar of your browser.') . '</p>         
        </div>
        <div class="step-2">
          <h4>' . t('Step %number', array('%number' => 2)) .'</h4><br />
          <img border="1" src="' . $path . '/images/howto/firefox/site_security.png"/>
          <p>' . t('You can check that the website url indicates the right website and make a click on "View certificate".') . '</p>
        </div>
        <div class="step-3">
          <h4>' . t('Step %number', array('%number' => 3)) .'</h4><br />
          <img border="1" src="' . $path . '/images/howto/firefox/certificate.png"/>
          <p>' . t('Compare the SHA1 fingerprint with the one displayed on the buttom of the page.') . '</p>         
        </div>
      </div>
      </fieldset>';

  // Chorme
  $output .=
    '<fieldset id="ssl-verification-chrome" class="collapsible collapsed">
<legend><span class="fieldset-legend"><img src="' . $path . '/images/logos/chrome.png" alt="Chrome" title="Chrome" /></span></legend>
<div class="fieldset-wrapper">
<div class="fieldset-description"></div>
        <div class="step-1">
          <h4>' . t('Step %number', array('%number' => 1)) .'</h4><br />
          <img border="1" src="' . $path . '/images/howto/chrome/dropdown-permissions.png"/>
          <p>' . t('Click on the little lock symbol left to the url in the navigation bar of your browser') . '</p>
          </div>
        <div class="step-2">
          <h4>' . t('Step %number', array('%number' => 2)) .'</h4><br />
          <img border="1" src="' . $path . '/images/howto/chrome/dropdown-connection.png"/>
          <p>' . t('Switch to the nex tab "Connection" and proceed by clicking on "View certificate data".') . '</p>
        </div>
        <div class="step-3">
          <h4>' . t('Step %number', array('%number' => 3)) .'</h4><br />
          <img border="1" src="' . $path . '/images/howto/windows/certificate-general.png"/>
          <p>' . t('On the new window you can check that the website url indicates the right website and make a click on "Details".') . '</p>
        </div>
        <div class="step-4">
          <h4>' . t('Step %number', array('%number' => 4)) .'</h4><br />
          <img border="1" src="' . $path . '/images/howto/windows/certificate-details.png"/>
          <p>' . t('Compare the fingerprint (last item) with the one displayed on the buttom of the page.') . '</p>
        </div>
</div>
</fieldset>';

  // Internet Explorer
  $output .=     
    '<fieldset id="ssl-verification-ie" class="collapsible collapsed">
<legend><span class="fieldset-legend"><img src="' . $path . '/images/logos/ie.png" alt="Internet Explorer" title="Internet Explorer" /></span></legend>
<div class="fieldset-wrapper">
<div class="fieldset-description"></div>
        <div class="step-1">
          <h4>' . t('Step %number', array('%number' => 1)) .'</h4><br />
          <img border="1" src="' . $path . '/images/howto/ie/dropdown.png"/>
          <p>' . t('Click on the little symbol left to the url in the navigation bar of your browser and proceed by clicking on "View certificates".') . '</p>         
        </div>
        <div class="step-2">
          <h4>' . t('Step %number', array('%number' => 2)) .'</h4><br />
          <img border="1" src="' . $path . '/images/howto/windows/certificate-general.png"/>
          <p>' . t('On the new window you can check that the website url indicates the right website and make a click on "Details".') . '</p>
        </div>
        <div class="step-3">
          <h4>' . t('Step %number', array('%number' => 3)) .'</h4><br />
          <img border="1" src="' . $path . '/images/howto/windows/certificate-details.png"/>
          <p>' . t('Compare the fingerprint (last item) with the one displayed on the buttom of the page.') . '</p>         
        </div>

</div>
</fieldset>';

return $output;
}


/**
 * Themes the obtained fingerprint.
 */
function theme_ssl_verification_fingerprint($fingerprint) {

  $output = '<strong>SHA1:</strong>';
  $output .= '<div class="ssl-verification-fingerprint">' . chunk_split($fingerprint['sha1'], 2, ' '). '</div>';
  return $output;
}
